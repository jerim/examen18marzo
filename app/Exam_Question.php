<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam_Question extends Model
{
  public function exam_id()
    {
        return $this->belongsTo(Exam::class);
    }
      public function question_id()
    {
        return $this->belongsTo(Question::class);
    }
}


