<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
     protected $fillable = [
        'id', 'text', 'a','b','c','d','answer'
    ];

      public function module_id()
    {
        return $this->belongsTo(Module::class);
    }
}

