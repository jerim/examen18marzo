<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
     protected $fillable = [
        'id', 'title',
    ];

      public function materia_id()
    {
        return $this->belongsTo(Module::class);
    }

      public function user_id()
    {
        return $this->belongsTo(User::class);
    }
}


